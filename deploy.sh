#!/bin/sh

ssh -o StrictHostKeyChecking=no ubuntu@$EC2_PUBLIC_IP_ADDRESS << 'ENDSSH'
  cd /home/ubuntu/ecommerce
  export $(cat .env | xargs)
  aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 067149222081.dkr.ecr.eu-west-1.amazonaws.com
  docker pull $IMAGE:web
  docker pull $IMAGE:frontend
  docker-compose up -d
ENDSSH