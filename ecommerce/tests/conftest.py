from pytest_factoryboy import register

from .factories import ReviewFactory

register(ReviewFactory, 'review_low_rating')
register(ReviewFactory, 'review_high_rating', rating=5)
