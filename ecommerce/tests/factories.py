import factory


class ReviewFactory(factory.django.DjangoModelFactory):
    title = "Review title"
    desc = "Review desc"
    rating = 1

    class Meta:
        model = "store_ecommerce.Review"
