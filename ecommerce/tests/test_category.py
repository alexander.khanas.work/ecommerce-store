import pytest

from store_ecommerce.models import Category


@pytest.fixture
def category_factory(db):
    def create_category(name, parent=None):
        return Category.objects.create(name=name, parent=parent)

    return create_category


@pytest.fixture
def category_a(db, category_factory):
    return category_factory('A')


@pytest.fixture
def category_b(db, category_factory):
    return category_factory('B')


@pytest.fixture
def category_c(db, category_factory):
    return category_factory('C')


@pytest.mark.parametrize('name',
                         ['Title 1', 'Title 2', 'Title 3'])
def test_categories_create(category_factory, name):
    category = category_factory(name)
    assert category.name == name


def test_should_create_two_categories(category_a, category_b):
    print('category a ===', category_a.pk)
    assert category_a.pk != category_b.pk


def test_should_create_category_with_correct_name(category_a):
    assert category_a.name == 'A'
