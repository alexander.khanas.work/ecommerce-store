from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase, APIClient


class CategoryTestCase(APITestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.client = APIClient()

    def test_category_create(self):
        # Prepare data
        body = {
            'name': 'Category',
        }
        # Make request
        response = self.client.post('/categories/?page=1', body, follow=True)
        # Check status response
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
