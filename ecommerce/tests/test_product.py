import pytest

from store_ecommerce.models import Product, Category, Review, CustomUser


@pytest.fixture
def product_factory(db):
    user = CustomUser.objects.create(email='johndoe@gmail.com')
    category = Category.objects.create(name='Category A and B')
    review = Review.objects.create(title='Review Title', desc='Review Desc', user=user, rating=3)

    def create_product(name, desc):
        product = Product.objects.create(name=name, desc=desc, category=category)
        product.reviews.set([review])
        return product

    return create_product


@pytest.fixture
def product_a(db, product_factory):
    return product_factory(name='A', desc='A desc')


@pytest.fixture
def product_b(db, product_factory):
    return product_factory(name='B', desc='Desc')


def test_should_create_two_products(product_a, product_b):
    print('product a ===', product_a.pk)
    assert product_a.pk != product_b.pk


def test_should_create_product_with_correct_name(product_a):
    assert product_a.name == 'A'
