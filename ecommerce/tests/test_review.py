import pytest


@pytest.mark.django_db
def test_reviews(review_low_rating, review_high_rating):
    assert review_low_rating.rating != review_high_rating.rating