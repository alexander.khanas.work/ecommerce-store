from unittest.mock import patch
from nose.tools import assert_is_not_none
from store_ecommerce.external_apis import get_weather


@patch('store_ecommerce.external_apis.requests.get')
def test_getting_weather(mock_get):
    mock_get.return_value.ok = True
    response = get_weather()
    assert_is_not_none(response)