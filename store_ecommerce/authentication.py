from rest_framework.authentication import BaseAuthentication
from rest_framework import exceptions
from django.contrib.auth import get_user_model
import datetime
import jwt
from django.conf import settings


class SafeJWTAuthentication(BaseAuthentication):

    def authenticate(self, request):

        user_model = get_user_model()
        authorization_header = request.headers.get('Authorization')

        if not authorization_header:
            return None
        try:
            access_token = authorization_header.split(' ')[1]
            payload = jwt.decode(
                access_token, settings.SECRET_KEY, algorithms=['HS256'])

        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed('Token expired')

        user = user_model.objects.get(id=payload['user_id'])
        if user is None:
            raise exceptions.AuthenticationFailed('User not found')

        return user, None


class TokenCreator:
    user = None

    def __init__(self, user_id):
        UserModel = get_user_model()
        user = UserModel.objects.get(id=user_id)
        self.user = user

    def generate_access_token(self):
        access_token_payload = {
            'user_id': self.user.id,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=5),
            'iat': datetime.datetime.utcnow(),
        }
        access_token = jwt.encode(access_token_payload,
                                  settings.SECRET_KEY, algorithm='HS256')
        return access_token

    def generate_refresh_token(self):
        refresh_token_payload = {
            'user_id': self.user.id,
            'exp': datetime.datetime.utcnow() + datetime.timedelta(days=7),
            'iat': datetime.datetime.utcnow()
        }
        refresh_token = jwt.encode(
            refresh_token_payload, settings.REFRESH_TOKEN_SECRET, algorithm='HS256')

        return refresh_token
