import requests


def get_weather():
    response = requests.get('api.openweathermap.org/data/2.5/weather?q=london&appid=7467d58fb05a903ed31582acb954db4f')
    if response.ok:
        return response
    else:
        return None