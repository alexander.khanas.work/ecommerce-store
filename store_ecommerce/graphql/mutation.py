import graphene
import graphql_jwt

from store_ecommerce.graphql.types import CategoryType, UserType
from store_ecommerce.models import Category, Product
from django.contrib.auth import get_user_model


class CreateUser(graphene.Mutation):
    user = graphene.Field(UserType)

    class Arguments:
        password = graphene.String(required=True)
        email = graphene.String(required=True)

    def mutate(self, info, password, email):
        user = get_user_model()(
            email=email,
        )
        user.set_password(password)
        user.save()

        return CreateUser(user=user)


class CategoryInput(graphene.InputObjectType):
    id = graphene.ID()
    name = graphene.String()
    parent = graphene.ID()


class CreateCategory(graphene.Mutation):
    class Arguments:
        category_data = CategoryInput(required=True)

    category = graphene.Field(CategoryType)

    @staticmethod
    def mutate(root, info, category_data=None):
        category_instance = Category(
            title=category_data.title,
            author=category_data.author,
            year_published=category_data.year_published,
            review=category_data.review
        )
        category_instance.save()
        return CreateCategory(category=category_instance)


class DeleteProduct(graphene.Mutation):
    class Arguments:
        product_id = graphene.ID()

    @staticmethod
    def mutate(self, product_id=None):
        product = Product.objects.get(id=product_id)
        product.delete()
        return DeleteProduct(id=product_id)


class ObtainJSONWebToken(graphql_jwt.JSONWebTokenMutation):
    user = graphene.Field(UserType)

    @classmethod
    def resolve(cls, root, info, **kwargs):
        return cls(user=info.context.user)


class Mutation(graphene.ObjectType):
    create_category = CreateCategory.Field()
    create_user = CreateUser.Field()
    token_auth = ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
