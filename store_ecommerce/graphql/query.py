import graphene
from django.contrib.auth import get_user_model
from graphql import GraphQLError

from store_ecommerce.graphql.types import UserType, CategoryType, ReviewType, ProductType
from store_ecommerce.models import CustomUser, Category, Review, Product


class Query(graphene.ObjectType):
    user = graphene.Field(UserType, id=graphene.Int())
    users = graphene.List(UserType)
    category = graphene.Field(CategoryType, id=graphene.Int())
    categories = graphene.List(CategoryType)
    review = graphene.Field(ReviewType, id=graphene.Int())
    reviews = graphene.List(ReviewType)
    product = graphene.Field(ProductType, id=graphene.Int())
    products = graphene.List(ProductType, category=graphene.Int())

    def resolve_user(self, info, user_id):
        user = info.context.user
        if user.is_anonymous:
            return
        return CustomUser.objects.get(id=user_id)

    def resolve_user_by_email(self, info, email):
        return CustomUser.objects.get(email=email)

    def resolve_users(self, info):
        user = info.context.user
        if user.is_anonymous:
            return
        return get_user_model().objects.all()

    def resolve_category(self, info, category_id):
        user = info.context.user
        if user.is_anonymous:
            return
        return Category.objects.get(id=category_id)

    def resolve_categories(self, info):
        user = info.context.user
        if user.is_anonymous:
            return
        return Category.objects.all()

    def resolve_review(self, info, review_id):
        user = info.context.user
        if user.is_anonymous:
            return
        return Review.objects.get(id=review_id)

    def resolve_reviews(self, info):
        user = info.context.user
        if user.is_anonymous:
            return
        return Review.objects.all()

    def resolve_product(self, info, product_id):
        user = info.context.user
        if user.is_anonymous:
            return
        return Product.objects.get(id=product_id)

    def resolve_products(self, info, category):
        user = info.context.user
        if user.is_anonymous:
            raise GraphQLError(403)
        print(category)
        if category != 0:
            return Product.objects.filter(category=category)
        return Product.objects.all()
