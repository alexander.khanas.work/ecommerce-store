import graphene

from store_ecommerce.graphql.mutation import Mutation
from store_ecommerce.graphql.query import Query

schema = graphene.Schema(query=Query, mutation=Mutation)
