from django.contrib.auth import get_user_model
from graphene_django import DjangoObjectType

from store_ecommerce.models import CustomUser, Category, Review, Product


class UserType(DjangoObjectType):
    class Meta:
        model = CustomUser
        fields = "__all__"


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category
        fields = "__all__"


class ReviewType(DjangoObjectType):
    class Meta:
        model = Review
        fields = "__all__"


class ProductType(DjangoObjectType):
    class Meta:
        model = Product
        fields = "__all__"
