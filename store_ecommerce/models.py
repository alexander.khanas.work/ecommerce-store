from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.utils import timezone

from store_ecommerce.managers import CustomUserManager


class CustomUser(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    phone = models.IntegerField(blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    first_name = models.CharField(max_length=255, blank=True, null=True)
    last_name = models.CharField(max_length=255, blank=True, null=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email


class Category(models.Model):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, blank=True, null=True)


class Review(models.Model):
    rating = models.IntegerField()
    user = models.ForeignKey('CustomUser', on_delete=models.CASCADE, blank=True, null=True)
    desc = models.CharField(max_length=255)
    title = models.CharField(max_length=100)


class Product(models.Model):
    name = models.CharField(max_length=100)
    desc = models.CharField(max_length=255)
    category = models.ForeignKey('Category', on_delete=models.CASCADE, null=True, blank=True)
    reviews = models.ManyToManyField('Review', blank=True)
    photo_url = models.CharField(max_length=255)
