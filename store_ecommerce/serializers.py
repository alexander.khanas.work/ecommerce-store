from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.contrib.auth.hashers import make_password
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from store_ecommerce.models import Category, Product, Review


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):

    @classmethod
    def get_token(cls, user):
        token = super(MyTokenObtainPairSerializer, cls).get_token(user)

        # Add custom claims
        token['email'] = user.email
        token['phone'] = user.phone
        token['birthday'] = user.birthday
        token['first_name'] = user.first_name
        token['last_name'] = user.last_name
        token['website_link'] = user.website_link
        token['nickname'] = user.nickname
        return token


class UserSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)
    id = serializers.IntegerField(required=False)
    phone = serializers.IntegerField(allow_null=True, required=False)
    first_name = serializers.CharField(allow_null=True, required=False)
    last_name = serializers.CharField(allow_null=True, required=False)
    birthday = serializers.DateField(allow_null=True, required=False)
    thumbnail = serializers.CharField(allow_null=True, required=False)

    def create(self, validated_data):
        email = validated_data['email']
        password = make_password(validated_data['password'])
        print('password ===', password)
        return get_user_model().objects.create(email=email, password=password)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.password = validated_data.get('password', instance.password)
        instance.phone = validated_data.get('phone', instance.phone)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.birthday = validated_data.get('birthday', instance.birthday)
        instance.thumbnail = validated_data.get('thumbnail', instance.thumbnail)
        instance.save()
        return instance


class CategorySerializer(serializers.ModelSerializer):
    def validate_name(self, data):
        if len(data) < 5:
            raise serializers.ValidationError('Error category name should be longer than 5 symbols')
        return data

    class Meta:
        model = Category
        fields = '__all__'


class ReviewSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        depth = 1
        model = Review
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'


class DetailProductSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    reviews = ReviewSerializer(many=True)

    class Meta:
        model = Product
        depth = 1
        fields = (
            '__all__'
        )
