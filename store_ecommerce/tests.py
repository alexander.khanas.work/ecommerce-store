import pytest

from .models import Category


@pytest.fixture
def category_factory(db):
    def create_category(name, parent=None):
        return Category.objects.create(name=name, parent=parent)

    return create_category


@pytest.fixture
def category_a(db, category_factory):
    return category_factory('A')


@pytest.fixture
def category_b(db, category_factory):
    return category_factory('B')


def test_should_create_two_categories(category_a, category_b):
    print('category a ===', category_a.pk)
    assert category_a.pk != category_b.pk


def test_should_create_category_with_correct_name(category_a):
    assert category_a.name == 'A'
