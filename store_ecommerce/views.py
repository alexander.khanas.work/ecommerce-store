import jwt
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import check_password
from django.shortcuts import render
from rest_framework import status
from rest_framework import viewsets
from rest_framework.renderers import StaticHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import get_object_or_404

from ecommerce.celery import send_verification_email
from store_ecommerce.models import Category, Product, Review
from store_ecommerce.serializers import CategorySerializer, UserSerializer, \
    ProductSerializer, ReviewSerializer, DetailProductSerializer
from store_ecommerce.authentication import TokenCreator

from django.http import HttpResponse
from django.views.generic import View
from django.conf import settings

import logging


class LoginView(APIView):
    def post(self, request):
        email = request.data.get('email')
        user = get_user_model().objects.get(email=email)
        password = request.data.get('password')
        serializer = UserSerializer(data=request.data)
        print(check_password(password, user.password))
        if serializer.is_valid() and check_password(password, user.password):
            token_creator = TokenCreator(user.id)
            refresh = token_creator.generate_refresh_token()
            access = token_creator.generate_access_token()
            return Response({"user": {**serializer.data, "password": None}, "id": user.id, "refresh": str(refresh), "token": str(access)})
        return Response(status=status.HTTP_400_BAD_REQUEST, data=serializer.errors)


class RegisterView(APIView):
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            print(serializer.data['id'])
            token_creator = TokenCreator(serializer.data['id'])
            refresh = token_creator.generate_refresh_token()
            access = token_creator.generate_access_token()
            send_verification_email(serializer.data['id'])
            return Response({"user": serializer.data, "refresh": str(refresh), "token": str(access)})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class RefreshView(APIView):
    authentication_classes = []  # disables authentication
    permission_classes = []  # disables permission

    def get(self, request):
        user_model = get_user_model()
        refresh_token = request.headers.get('Authorization')

        if refresh_token is None:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        refresh_token = refresh_token.split(' ')[1]
        print('refresh token ===', refresh_token)
        try:
            payload = jwt.decode(
                refresh_token, settings.REFRESH_TOKEN_SECRET, algorithms=['HS256'])
        except jwt.ExpiredSignatureError:
            return Response(status=status.HTTP_403_FORBIDDEN)

        user = user_model.objects.get(id=payload.get('user_id'))
        if user is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        token_creator = TokenCreator(user)
        access_token = token_creator.generate_access_token()
        return Response({'token': access_token})


class ReviewView(viewsets.ModelViewSet):
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    renderer_classes = [StaticHTMLRenderer, ]

    def destroy(self, request, pk, **kwargs):
        product = get_object_or_404(self.get_queryset(), pk=pk)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class ProductView(viewsets.ModelViewSet):
    queryset = Product.objects.all()

    serializer_classes = {
        'list': DetailProductSerializer,
        'retrieve': DetailProductSerializer,
        'create': ProductSerializer
    }

    default_serializer_class = ProductSerializer

    def get_serializer_class(self):
        print(self.action)
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    def get(self):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        response = None
        if page is not None:
            serializer = DetailProductSerializer(page, many=True)
            result = self.get_paginated_response(serializer.data)
            response = Response(result.data)  # pagination data
        else:
            return Response(
                {"message": "you should add 'page' query to your request url"},
                status=status.HTTP_400_BAD_REQUEST
            )

        response.set_cookie('cookie', 'cookie_value')
        return response

    def destroy(self, request, pk, **kwargs):
        product = get_object_or_404(self.get_queryset(), pk=pk)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class CategoryView(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    renderer_classes = [JSONRenderer, ]

    def destroy(self, request, pk, **kwargs):
        product = get_object_or_404(self.get_queryset(), pk=pk)
        product.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class FrontendAppView(View):
    """
    Serves the compiled frontend entry point (only works if you have run `yarn
    run build`).
    """
    def get(self, request):
        try:
            return render(request, 'index.html')
        except FileNotFoundError:
            logging.exception('Production build of app not found')
            return HttpResponse(
                """
                This URL is only used when you have built the production
                version of the app. Visit http://localhost:3000/ instead, or
                run `yarn run build` to test the production version.
                """,
                status=501,
            )
